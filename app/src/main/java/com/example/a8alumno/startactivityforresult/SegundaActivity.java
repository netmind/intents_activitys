package com.example.a8alumno.startactivityforresult;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.widget.TextView;

public class SegundaActivity extends AppCompatActivity {

    private String value;
    private TextView resultIntent;
    private Casa casa;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_segunda);
        if (getIntent().getExtras() != null) {
            casa = getIntent().getExtras().getParcelable(ConstantsObjects.ID_OBJECT_CASA);
            value = getIntent().getExtras().getString("string_value");
        }
        bindViews();

        resultIntent.setText(String.valueOf(casa.getHombre()));
    }

    private void bindViews() {
        resultIntent = (TextView) findViewById(R.id.segunda_activity_txt_result_intent);
    }
}
