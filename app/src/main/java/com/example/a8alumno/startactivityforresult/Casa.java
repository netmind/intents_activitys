package com.example.a8alumno.startactivityforresult;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by A8Alumno on 07/10/2017.
 */

public class Casa implements Parcelable {
    private String muebles;
    private String television;
    private int piezasRopa;
    private int hombre;
    private boolean cocina;

    public Casa() {
    }

    public String getMuebles() {
        return muebles;
    }

    public void setMuebles(String muebles) {
        this.muebles = muebles;
    }

    public String getTelevision() {
        return television;
    }

    public void setTelevision(String television) {
        this.television = television;
    }

    public int getPiezasRopa() {
        return piezasRopa;
    }

    public void setPiezasRopa(int piezasRopa) {
        this.piezasRopa = piezasRopa;
    }

    public int getHombre() {
        return hombre;
    }

    public void setHombre(int hombre) {
        this.hombre = hombre;
    }

    public boolean isCocina() {
        return cocina;
    }

    public void setCocina(boolean cocina) {
        this.cocina = cocina;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.muebles);
        dest.writeString(this.television);
        dest.writeInt(this.piezasRopa);
        dest.writeInt(this.hombre);
        dest.writeByte(this.cocina ? (byte) 1 : (byte) 0);
    }

    protected Casa(Parcel in) {
        this.muebles = in.readString();
        this.television = in.readString();
        this.piezasRopa = in.readInt();
        this.hombre = in.readInt();
        this.cocina = in.readByte() != 0;
    }

    public static final Parcelable.Creator<Casa> CREATOR = new Parcelable.Creator<Casa>() {
        @Override
        public Casa createFromParcel(Parcel source) {
            return new Casa(source);
        }

        @Override
        public Casa[] newArray(int size) {
            return new Casa[size];
        }
    };
}
