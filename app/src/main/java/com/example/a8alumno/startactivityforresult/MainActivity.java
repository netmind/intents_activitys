package com.example.a8alumno.startactivityforresult;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {

    private static final int SUBACTIVITY = 1;
    private Button btnNewActivity;
    private Button btnDisplayInfo;
    private TextView txtResult;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        bindViews();
    }

    private void bindViews() {
        btnNewActivity = (Button) findViewById(R.id.mainactivity_btn_new_activity);
        btnDisplayInfo = (Button) findViewById(R.id.mainactivity_btn_display_info);
        txtResult = (TextView) findViewById(R.id.mainactivity_txt_result_display);
        setUpListeners();
    }

    private void setUpListeners() {
        //BTN LISTENER ONCLICK NEW ACTIVITY
        btnNewActivity.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Casa casa = new Casa();
                casa.setCocina(true);
                casa.setHombre(2);
                casa.setMuebles("comedor");
                casa.setPiezasRopa(155);
                Intent goActivity = new Intent(MainActivity.this, SegundaActivity.class);
                goActivity.putExtra("string_value", "hola soy yo");
                goActivity.putExtra(ConstantsObjects.ID_OBJECT_CASA, casa);
                startActivity(goActivity);
            }
        });

        //BTN LISTENER ONCLICK DISPLAY
        btnDisplayInfo.setOnClickListener(this);
    }


    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.mainactivity_btn_display_info:
                Toast.makeText(MainActivity.this, "MAS TARDE SE IMPLEMENTARA", Toast.LENGTH_LONG).show();
                /*
                Intent goActivity= new Intent(MainActivity.this,SegundaActivity.class);
                startActivityForResult(goActivity,SUBACTIVITY);
                */
                break;
        }
    }


}
